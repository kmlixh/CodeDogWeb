import Vue from 'vue'
import Router from 'vue-router'
const Articles = resolve => require(['@/components/Articles'], resolve)
const NotFind = resolve => require(['@/components/NotFind'], resolve)
const LoginAdmin = resolve => require(['@/components/LoginAdmin'], resolve)
const Admin = resolve => require(['@/components/Admin'], resolve)
const AdminArticles = resolve => require(['@/components/AdminArticleList'], resolve)
const ArticleInfo = resolve => require(['@/components/ArticleInfo'], resolve)
const EditArticle = resolve => require(['@/components/EditArticle'], resolve)
const EditKeyword = resolve => require(['@/components/EditKeyword'], resolve)
const Vuetify = resolve => require(['@/components/Vuetify'], resolve)

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path:'*',
      name:'404',
      component:NotFind
    },
    {
      path: '/index',
      name: 'index',
      component: Articles
    },
    {
      path: '/vuetify',
      name: 'vuetify',
      component: Vuetify
    },
    {
      path: '/',
      name: 'home',
      component: Articles
    },
    {
      path: '/articles/:id',
      name: 'articles',
      component: Articles
    },
    {
      path:"/admin/login",
      name:"loginAdmin",
      component:LoginAdmin
    },
    {
      path:"/admin",
      name:"adminCenter",
      component:Admin
    },
    {
      path:"/admin/articles",
      name:"newArticle",
      component:AdminArticles
    },
    {
      path:"/admin/editArticle/:id",
      name:"editArticle",
      component:EditArticle
    },
    {
      path:"/admin/editArticle",
      name:"adminEditArticle",
      component:EditArticle
    },
    {
      path:"/article/:id",
      name:"articleInfo",
      component:ArticleInfo
    },
    {
      path:"/admin/editKeyword",
      name:"editKeyword",
      component:EditKeyword
    }
  ]
})
