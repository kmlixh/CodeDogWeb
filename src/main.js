// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import '../static/theme/index.css'
import '../static/vuetify.min.css'
import '../static/google.css'
import ElementUI from 'element-ui'
import App from './App'
import router from './router'
import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import Vuetify from 'vuetify'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(Vuetify)
Vue.use(mavonEditor)

/* eslint-disable no-new */
var vue=new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
